import { Router } from "express";
import { validateEmail } from "../controllers/validateEmail.controller";

const validateEmailRoutes = Router();
validateEmailRoutes.post('/', validateEmail)

export {
    validateEmailRoutes
}