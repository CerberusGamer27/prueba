import net from 'node:net';

enum SMTPStageNames {
    CHECK_CONNECTION_ESTABLISHIED = 'CHECK_CONNECTION_ESTABLISHED',
    SEND_EHLO = 'SEND_EHLO',
    SEND_MAIL_FROM = 'SEND_MAIL_FROM',
    SEND_RECIPIENT_TO = 'SEND_RECIPIENT_TO'

}

export type TTestInboxResult = {
    connection_succeeded: boolean,
    inbox_exist: boolean 
}

export const testInboxOnServer = async (smtpHostname: string, emailInbox: string): Promise<TTestInboxResult> => {
    return new Promise((resolve, rejects) => {
        const result = {
            connection_succeeded: true,
            inbox_exist: false
        }
        const socket = net.createConnection(25, smtpHostname)
        let currentStageName: SMTPStageNames = SMTPStageNames.CHECK_CONNECTION_ESTABLISHIED

        socket.on('data', (data: Buffer) => {
            const response = data.toString('utf-8')

            console.log('<--' + response)
            
            switch(currentStageName){
                case SMTPStageNames.CHECK_CONNECTION_ESTABLISHIED: {
                    const expectedReplyCode = '220'
                    const nextStageName = SMTPStageNames.SEND_EHLO
                    const command = `EHLO mail.example.org\r\n`

                    if(!(response.startsWith(expectedReplyCode))){
                        console.error(response)
                        socket.end();
                        return resolve(result);
                    }

                    result.connection_succeeded = true;

                    socket.write('command', () => {
                        console.log('-->' + command)
                        currentStageName = nextStageName
                    })

                    break
                }

                case SMTPStageNames.SEND_EHLO: {
                    const expectedReplyCode = '250'
                    const nextStageName = SMTPStageNames.SEND_MAIL_FROM
                    const command = `MAIL FROM:<name@example.org>\r\n`

                    if(!(response.startsWith(expectedReplyCode))){
                        console.error(response)
                        socket.end();
                        return resolve(result);
                    }

                    socket.write('command', () => {
                        console.log('-->' + command)
                        currentStageName = nextStageName
                    })

                    break
                }

                case SMTPStageNames.SEND_MAIL_FROM: {
                    const expectedReplyCode = '250'
                    const nextStageName = SMTPStageNames.SEND_RECIPIENT_TO
                    const command = `RCPT TO:<${emailInbox}>\r\n`

                    if(!(response.startsWith(expectedReplyCode))){
                        console.error(response)
                        socket.end();
                        return resolve(result);
                    }
                    socket.write('command', () => {
                        console.log('-->' + command)
                        currentStageName = nextStageName
                    })

                    break
                }

                case SMTPStageNames.SEND_RECIPIENT_TO: {
                    const expectedReplyCode = '250'
                    const command = `QUIT\r\n`

                    if(!(response.startsWith(expectedReplyCode))){
                        console.error(response)
                        socket.end();
                        return resolve(result);
                    }

                    result.inbox_exist = true;

                    socket.write('command', () => {
                        console.log('-->' + command)
                        socket.end();
                        return resolve(result)
                    })
                }
            }
        })

        socket.on('error', (err: Error) => {
            console.error(err);
            rejects(err)
        })

        socket.on('connect', () => {
            console.log('Connected to: '+ smtpHostname);
        })
    })
}