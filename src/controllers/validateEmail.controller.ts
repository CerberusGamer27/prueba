import { randomBytes } from "crypto";
import { Request, Response, NextFunction } from "express"
import { resolveMxRecords } from "../services/resolveMxRecords.service";
import { testInboxOnServer, TTestInboxResult } from "../services/testInboxOnServer.service";
import { verifyEmailFormat } from "../services/verifyEmailFormat.service";

export const validateEmail = async (req: Request, res: Response, next: NextFunction) => {
    console.log(req.body)
    if(!(req.body?.email)){
        res.status(400).json({error: 'Missing Email'})
        return next()
    }   

    const emailFomatIsValid = verifyEmailFormat(req.body.email)
    if(!emailFomatIsValid){
        res.status(400).json({error: 'Email format is not valid'})
        return next()
    }

    const [, domain] = req.body.email.split('@');

    const mxRecord = await resolveMxRecords(domain);
    console.log(mxRecord);
    const sortedMxRecords = mxRecord.sort((a,b) => a.priority - b.priority)

    let smtpResult: TTestInboxResult = {connection_succeeded: false, inbox_exist: false}
    let hostIndex = 0

    while(hostIndex < sortedMxRecords.length){
        try {
            smtpResult = await testInboxOnServer(sortedMxRecords[hostIndex].exchange, req.body.email)
            if(!(smtpResult.connection_succeeded)){
                hostIndex++
            } else {
                break
            }
        } catch (error) {
            console.error(error)
        }
    }

    let useCatchAll = false
    try{
        const testCatchAll = await testInboxOnServer(sortedMxRecords[hostIndex].exchange, `${randomBytes(20).toString('hex')}}@${domain}`)
        useCatchAll = testCatchAll.inbox_exist
    } catch(error){
        console.error(error);
    }

    res.json({
        email_format_is_valid: emailFomatIsValid,
        uses_catch_all: useCatchAll,
        ...smtpResult
    })
    next()
}
