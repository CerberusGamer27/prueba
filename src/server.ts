import bodyParser from "body-parser";
import express from "express";
import { validateEmailRoutes } from "./routes/validateEmail.routes";

const PORT = 3000;
const app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(validateEmailRoutes);



app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`)
})
